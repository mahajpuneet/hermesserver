import requests
import time
import json

url = 'http://localhost:8080/assign/user/'

# Function to create a POST request
def create_variation(user_id, experiment_name):
    data = {
        'experimentName': experiment_name
    }
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url + str(user_id), data=json.dumps(data), headers=headers)
    return response

# Main loop to create 1000 variations
for i in range(1, 1001):
    response = create_variation(i, 'testPuneet')
    print(f"Request {i}: Status Code: {response.status_code}")
    time.sleep(1)  # Wait for 1 second before next request
