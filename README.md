# Hermes Testing Platform

## Overview
Hermes Testing Platform is an A/B testing framework that facilitates the creation, management, and analysis of experiments. This platform includes a Go server for backend operations and uses Python for data generation and analysis.

## Key Components

### Go Server
- Manages A/B testing experiments.
- Handles user assignment to experiment variations.
- Provides endpoints for fetching experiment details and statistics.

### Python Scripts
- `populator.py`: Populates initial data into the database.
- `redisCopy.py`: Transfers data from PostgreSQL to Redis.
- `dummyEventData.py`: Generates synthetic event data for testing.
- `dummyRedisForExperiment.py`: Simulates user assignments for experiments.
- `dailyExperimentAnalysisRun.py`: Analyzes experiment data and calculates key metrics.

## Installation

### Prerequisites
- Go (version 1.15 or higher)
- Python (version 3.8 or higher)
- PostgreSQL
- Redis

### Setup
1. Clone the repository:
   ```shell
   git clone [repository-url]
