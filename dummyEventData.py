import pandas as pd
import psycopg2
from faker import Faker
import random
from urllib.parse import urlparse

# Parse the provided PostgreSQL URL
def parse_postgres_url(url):
    result = urlparse(url)
    return {
        "host": result.hostname,
        "database": result.path[1:],  # Skip the leading '/'
        "user": result.username,
        "password": result.password,
        "port": result.port
    }

import json

# Create dummy data
def create_dummy_data(num_rows):
    fake = Faker()
    data = {
        "event_id": range(1, num_rows + 1),
        "uid": [fake.uuid4() for _ in range(num_rows)],
        "event_type": [random.choice(["click", "view", "purchase"]) for _ in range(num_rows)],
        "event_timestamp": [fake.date_time_this_year() for _ in range(num_rows)],
        "session_id": [fake.uuid4() for _ in range(num_rows)],
        "page_id": [fake.uuid4() for _ in range(num_rows)],
        "additional_properties": [json.dumps({"text": fake.sentence()}) for _ in range(num_rows)]  # Generates random JSON with text
    }
    return pd.DataFrame(data)

# Function to insert data into the database
def insert_data(df, conn):
    cursor = conn.cursor()
    for i, row in df.iterrows():
        sql = """INSERT INTO UserEvents (event_id, uid, event_type, event_timestamp, session_id, page_id, additional_properties) 
                 VALUES (%s, %s, %s, %s, %s, %s, %s)"""
        cursor.execute(sql, tuple(row))
    conn.commit()
    cursor.close()

# Main function
def main():
    # Your PostgreSQL URL
    postgres_url = "postgres://vultradmin:AVNS_c6g3wFNorBbOZrxBidq@vultr-prod-44079b59-5315-41bb-8359-7bb25c3d9946-vultr-prod-04c1.vultrdb.com:16751/defaultdb"

    # Parse the URL to get connection parameters
    db_config = parse_postgres_url(postgres_url)

    # Generate dummy data
    df = create_dummy_data(10000)

    # Connect to the database
    try:
        conn = psycopg2.connect(**db_config)
        print("Connection established")

        # Insert data into the database
        insert_data(df, conn)
        print("Data inserted successfully")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        if conn is not None:
            conn.close()
            print("Database connection closed")

if __name__ == "__main__":
    main()
