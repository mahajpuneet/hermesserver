import redis
import psycopg2
from datetime import datetime

# Connect to Redis
redis_url = "rediss://default:AVNS_DVckiitDVxpjeRapPuJ@vultr-prod-e11d062f-72a7-4c31-ba09-5f4f5194176a-vultr-prod-04c1.vultrdb.com:16752"
r = redis.from_url(redis_url)

# Connect to PostgreSQL
postgres_url = "postgres://vultradmin:AVNS_c6g3wFNorBbOZrxBidq@vultr-prod-44079b59-5315-41bb-8359-7bb25c3d9946-vultr-prod-04c1.vultrdb.com:16751/defaultdb"
conn = psycopg2.connect(postgres_url)
cursor = conn.cursor()

# Fetch data from Redis
keys = r.keys('user:*') + r.keys('device:*')
for key in keys:
    assignments = r.hgetall(key)
    for experiment, variation in assignments.items():
        # Prepare data for PostgreSQL
        assignment_date = datetime.now().date()
        uid = key.decode('utf-8').split(":")[1]  # Extract user/device ID
        experiment_name = experiment.decode('utf-8')
        bucket_variation = variation.decode('utf-8')

        # Insert data into PostgreSQL
        cursor.execute("INSERT INTO experiment_assignments (assignment_date, uid, experiment_name, bucket_variation) VALUES (%s, %s, %s, %s)",
                       (assignment_date, uid, experiment_name, bucket_variation))

# Commit changes and close connections
conn.commit()
cursor.close()
conn.close()
