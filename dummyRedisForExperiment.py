import psycopg2
import requests
import json
from urllib.parse import urlparse

# Parse the provided PostgreSQL URL
def parse_postgres_url(url):
    result = urlparse(url)
    return {
        "host": result.hostname,
        "database": result.path[1:],  # Skip the leading '/'
        "user": result.username,
        "password": result.password,
        "port": result.port
    }

# Function to create a POST request for bucket assignment
def create_variation(user_id, experiment_name):
    data = {'experimentName': experiment_name}
    headers = {'Content-Type': 'application/json'}
    response = requests.post(f'http://localhost:8080/assign/user/{user_id}', json=data, headers=headers)

    try:
        # Attempt to parse the response as JSON
        return response.json()
    except json.JSONDecodeError:
        # If response is not in JSON format, log the response for debugging
        print(f"Non-JSON response for user {user_id}: {response.text}")
        return None

# Main function
def main():
    # Your PostgreSQL URL
    postgres_url = "postgres://vultradmin:AVNS_c6g3wFNorBbOZrxBidq@vultr-prod-44079b59-5315-41bb-8359-7bb25c3d9946-vultr-prod-04c1.vultrdb.com:16751/defaultdb"
    
    # Parse the URL to get connection parameters
    db_config = parse_postgres_url(postgres_url)

    # Connect to the database
    try:
        conn = psycopg2.connect(**db_config)
        cursor = conn.cursor()
        print("Connection established")

        # Retrieve user IDs from UserEvents table
        cursor.execute("SELECT DISTINCT uid FROM UserEvents")
        user_ids = cursor.fetchall()  # Fetch all distinct user IDs

        # Assign each user to a bucket for 'sampleUserExp'
        for user_id in user_ids:
            response = create_variation(user_id[0], 'sampleUserExp')
            if response:
                print(f"User {user_id[0]} assigned to bucket: {response.get('bucket_variation', 'Unknown')}")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        if conn is not None:
            conn.close()
            print("Database connection closed")

if __name__ == "__main__":
    main()
