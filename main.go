package main

import (
	"context"
	"crypto/tls"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/spaolacci/murmur3"
)

// ExperimentRequest represents the expected structure of the JSON request body.
type ExperimentRequest struct {
	ExperimentName string `json:"experimentName"`
}

type Experiment struct {
	Name           string          `json:"name"`
	Type           string          `json:"type"`
	UuidPreference string          `json:"uuidPreference"`
	StartTime      time.Time       `json:"startTime"`
	StopTime       time.Time       `json:"stopTime"`
	AllUsers       bool            `json:"allUsers"`
	PercentUsers   string          `json:"percentUsers"`
	Strategy       json.RawMessage `json:"strategy"`
	Platforms      json.RawMessage `json:"platforms"`
	GoalMetrics    json.RawMessage `json:"goalMetrics"`
	Layers         json.RawMessage `json:"layers"`
	BucketGroup    json.RawMessage `json:"bucketGroup"` // Changed to slice of Variation
	RemoteConfig   json.RawMessage `json:"remoteConfig"`
	Audience       json.RawMessage `json:"audience"`
}

type Variation struct {
	Name           string  `json:"name"`
	TrafficPercent float64 `json:"trafficPercent"` // Ensure this aligns with your form structure
}

// IdentifierType represents the identifier of the user or device.
type IdentifierType string

const (
	IdentifierTypeUser   IdentifierType = "user"
	IdentifierTypeDevice IdentifierType = "device"
)

// Metric represents a metric used in an experiment.
type Metric struct {
	ID                    int             `json:"id"`
	Name                  string          `json:"name"`
	SortOrder             int             `json:"sortOrder"`
	SortBy                string          `json:"sortBy"`
	Description           string          `json:"description"`
	IsFinancial           bool            `json:"isFinancial"`
	MoreInfo              string          `json:"moreInfo"`
	DisplayName           string          `json:"displayName"`
	CalculateContribution bool            `json:"calculateContribution"`
	ShowPercentile        bool            `json:"showPercentile"`
	Properties            json.RawMessage `json:"properties"`
	RollUps               json.RawMessage `json:"rollUps"`
	Financial             bool            `json:"financial"`
}

// Layer represents a layer in an experiment.
type Layer struct {
	Name        string `json:"name"`
	DisplayName string `json:"displayName"`
}

// ExperimentStats represents the structure of the experiment analysis results.
type ExperimentStats struct {
	MetricName      string          `json:"metricName"`
	MetricValue     sql.NullFloat64 `json:"metricValue"`
	Delta           sql.NullFloat64 `json:"delta"`
	PValue          sql.NullFloat64 `json:"pValue"`
	BucketVariation string          `json:"bucketVariation"`
}

var (
	redisClient *redis.Client
	db          *sql.DB
	err         error
)

func enableCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("enableCORS middleware called") // Add this line
		// Set CORS headers
		w.Header().Set("Access-Control-Allow-Origin", "*") // Allow any origin
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

		// Check if the request is for a pre-flight OPTIONS request
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		// Pass down the request to the actual handler
		next.ServeHTTP(w, r)
	})
}

func init() {
	// Initialize the PostgreSQL connection
	db, err = sql.Open("postgres", "postgres://vultradmin:AVNS_c6g3wFNorBbOZrxBidq@vultr-prod-44079b59-5315-41bb-8359-7bb25c3d9946-vultr-prod-04c1.vultrdb.com:16751/defaultdb")
	if err != nil {
		log.Fatal(err)
	}

	// Check if the database is connected
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
}

// getMetrics handles the request to get metrics from the database.
func getMetrics(w http.ResponseWriter, r *http.Request) {
	// Create a slice to hold the metrics
	metrics := []Metric{}

	// Query the database for metrics
	rows, err := db.Query("SELECT id, name, sortOrder, sortBy, description, isFinancial, moreInfo, displayName, calculateContribution, showPercentile, properties, rollUps, financial FROM Metrics")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	// Iterate over the rows and populate the metrics slice
	for rows.Next() {
		var m Metric
		if err := rows.Scan(&m.ID, &m.Name, &m.SortOrder, &m.SortBy, &m.Description, &m.IsFinancial, &m.MoreInfo, &m.DisplayName, &m.CalculateContribution, &m.ShowPercentile, &m.Properties, &m.RollUps, &m.Financial); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		metrics = append(metrics, m)
	}

	// Check for errors from iterating over rows
	if err := rows.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Send a JSON response with the metrics
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(metrics); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func getLayers(w http.ResponseWriter, r *http.Request) {
	// Create a slice to hold the layers
	var layers []Layer

	// Query the database for layers
	rows, err := db.Query("SELECT name, displayName FROM Layers")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	// Iterate over the rows and populate the layers slice
	for rows.Next() {
		var layer Layer
		if err := rows.Scan(&layer.Name, &layer.DisplayName); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		layers = append(layers, layer)
	}

	// Check for errors from iterating over rows
	if err := rows.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Send a JSON response with the layers
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(layers); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Add this function to your main.go
func getExperiments(w http.ResponseWriter, r *http.Request) {
	var experiments []Experiment

	rows, err := db.Query("SELECT name, uuidPreference, startTime, stopTime FROM Experiment")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var exp Experiment
		if err := rows.Scan(&exp.Name, &exp.UuidPreference, &exp.StartTime, &exp.StopTime); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		experiments = append(experiments, exp)
	}

	if err := rows.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(experiments)
}

func getExperimentStats(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	experimentName := query.Get("name")

	var results []ExperimentStats
	rows, err := db.Query("SELECT metric_name, metric_value, delta, p_value, bucket_variation FROM experiment_analysis_results WHERE experiment_name = $1", experimentName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var stat ExperimentStats
		if err := rows.Scan(&stat.MetricName, &stat.MetricValue, &stat.Delta, &stat.PValue, &stat.BucketVariation); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		results = append(results, stat)
	}

	// Send a JSON response with the experiment stats
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(results); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// getValue converts sql.NullFloat64 to float64, returning 0 if it's NULL.
func getValue(nullFloat sql.NullFloat64) float64 {
	if nullFloat.Valid {
		return nullFloat.Float64
	}
	return 0
}

func calculateSlot(userDeviceID, experimentID string) int {
	input := userDeviceID + ":" + experimentID
	hasher := murmur3.New32()
	hasher.Write([]byte(input))
	hash := int(hasher.Sum32())
	return (hash % 100) + 1
}

func selectVariation(bucketGroup []Variation, identifier string, experimentName string) string {
	slot := calculateSlot(identifier, experimentName)
	cumulativePercentage := 0.0
	for _, variation := range bucketGroup {
		cumulativePercentage += variation.TrafficPercent
		if float64(slot) <= cumulativePercentage {
			return variation.Name
		}
	}
	if len(bucketGroup) > 0 {
		return bucketGroup[0].Name
	}
	return ""
}

func assign(w http.ResponseWriter, r *http.Request, identifierType IdentifierType) {
	vars := mux.Vars(r)
	identifier := vars["identifier"]

	var req ExperimentRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, "Failed to decode experiment request", http.StatusBadRequest)
		return
	}
	experimentName := req.ExperimentName

	var experiment Experiment
	var bucketGroupJSON []byte
	err := db.QueryRow("SELECT name, type, uuidPreference, startTime, stopTime, allUsers, percentUsers, platforms, goalMetrics, layers, bucketGroup, remoteConfig, audience FROM Experiment WHERE name = $1", experimentName).Scan(&experiment.Name, &experiment.Type, &experiment.UuidPreference, &experiment.StartTime, &experiment.StopTime, &experiment.AllUsers, &experiment.PercentUsers, &experiment.Platforms, &experiment.GoalMetrics, &experiment.Layers, &bucketGroupJSON, &experiment.RemoteConfig, &experiment.Audience)
	if err != nil {
		log.Printf("Error retrieving experiment: %v", err)
		http.Error(w, "Experiment not found", http.StatusNotFound)
		return
	}

	var bucketGroup []Variation
	if err := json.Unmarshal(bucketGroupJSON, &bucketGroup); err != nil {
		log.Printf("Error unmarshalling bucket group: %v", err)
		http.Error(w, "Error unmarshalling bucket group", http.StatusInternalServerError)
		return
	}

	variation := selectVariation(bucketGroup, identifier, experiment.Name)

	// Updated Redis storage using hash
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	redisKey := fmt.Sprintf("%s:%s", identifierType, identifier) // e.g., "user:12345"
	if err := redisClient.HSet(ctx, redisKey, experimentName, variation).Err(); err != nil {
		log.Printf("Failed to set assignment in Redis: %v", err)
		http.Error(w, "Failed to set assignment in Redis", http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "Assigned to variation: %s in experiment: %s\n", variation, experimentName)
}

func getUserAssignment(w http.ResponseWriter, r *http.Request, identifierType IdentifierType) {
	vars := mux.Vars(r)
	identifier := vars["identifier"]

	// Get the assigned variation
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	var key string
	switch identifierType {
	case IdentifierTypeUser:
		key = "user_assignments"
	case IdentifierTypeDevice:
		key = "device_assignments"
	}

	variation, err := redisClient.HGet(ctx, key, identifier).Result()
	if err != nil {
		http.Error(w, "User or device not found or not assigned to a variation", http.StatusNotFound)
		return
	}

	fmt.Fprintf(w, "Assigned variation: %s\n", variation)
}

func upsertExperiment(w http.ResponseWriter, r *http.Request) {
	log.Println("Came into upsert")

	query := r.URL.Query()
	experimentName := query.Get("name")

	// Create an Experiment instance and populate it
	var experiment Experiment
	experiment.Name = query.Get("name")
	experiment.UuidPreference = query.Get("uuidPreference")
	experiment.PercentUsers = query.Get("percentUsers")
	experiment.GoalMetrics = json.RawMessage(query.Get("goalMetrics"))
	experiment.Layers = json.RawMessage(query.Get("layers"))
	experiment.RemoteConfig = json.RawMessage(query.Get("remoteConfig"))
	experiment.Audience = json.RawMessage(query.Get("audience"))
	experiment.Strategy = json.RawMessage(query.Get("strategy"))
	experiment.Platforms = json.RawMessage(query.Get("platforms"))

	// Parsing time fields
	var err error
	experiment.StartTime, err = time.Parse("2006-01-02", query.Get("startTime"))
	if err != nil {
		log.Printf("Error parsing start time: %v", err)
		http.Error(w, "Invalid start time format", http.StatusBadRequest)
		return
	}

	experiment.StopTime, err = time.Parse("2006-01-02", query.Get("stopTime"))
	if err != nil {
		log.Printf("Error parsing stop time: %v", err)
		http.Error(w, "Invalid stop time format", http.StatusBadRequest)
		return
	}

	// Prepare JSON data for SQL insertion
	goalMetricsJSON := prepareJSONForSQL(experiment.GoalMetrics)
	layersJSON := prepareJSONForSQL(experiment.Layers)
	bucketGroupJSON := prepareJSONForSQL(json.RawMessage(query.Get("bucketGroup")))
	remoteConfigJSON := prepareJSONForSQL(experiment.RemoteConfig)
	audienceJSON := prepareJSONForSQL(experiment.Audience)
	strategyJSON := prepareJSONForSQL(json.RawMessage(query.Get("strategy")))
	platformsJSON := prepareJSONForSQL(json.RawMessage(query.Get("platforms")))

	var exists bool
	check_err := db.QueryRow("SELECT EXISTS(SELECT 1 FROM Experiment WHERE name = $1)", experimentName).Scan(&exists)
	if check_err != nil {
		http.Error(w, "Database error", http.StatusInternalServerError)
		return
	}

	var sqlStatement string
	if exists {
		sqlStatement = fmt.Sprintf(`UPDATE Experiment SET type = '%s', uuidPreference = '%s', startTime = '%s', stopTime = '%s', allUsers = %t, percentUsers = '%s', strategy = '%s', platforms = '%s', goalMetrics = '%s', layers = '%s', bucketGroup = '%s', remoteConfig = '%s', audience = '%s' WHERE name = '%s';`,
			experiment.Type, experiment.UuidPreference, experiment.StartTime.Format("2006-01-02"), experiment.StopTime.Format("2006-01-02"), experiment.AllUsers, experiment.PercentUsers, string(strategyJSON), string(platformsJSON), string(goalMetricsJSON), string(layersJSON), string(bucketGroupJSON), string(remoteConfigJSON), string(audienceJSON), experiment.Name)
	} else {
		sqlStatement = fmt.Sprintf(`INSERT INTO Experiment (name, type, uuidPreference, startTime, stopTime, allUsers, percentUsers, strategy, platforms, goalMetrics, layers, bucketGroup, remoteConfig, audience) VALUES ('%s', '%s', '%s', '%s', '%s', %t, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')`,
			experiment.Name, experiment.Type, experiment.UuidPreference, experiment.StartTime.Format("2006-01-02"), experiment.StopTime.Format("2006-01-02"), experiment.AllUsers, experiment.PercentUsers, string(strategyJSON), string(platformsJSON), string(goalMetricsJSON), string(layersJSON), string(bucketGroupJSON), string(remoteConfigJSON), string(audienceJSON))
	}
	log.Printf("Debug SQL: %s", sqlStatement)
	_, err = db.Exec(sqlStatement)
	if err != nil {
		http.Error(w, "Failed to create/update experiment", http.StatusInternalServerError)
		return
	}

	action := "created"
	if exists {
		action = "updated"
	}
	fmt.Fprintf(w, "Experiment %s successfully\n", action)
}

func prepareJSONForSQL(jsonData json.RawMessage) string {
	jsonDataStr := string(jsonData)

	// Return empty JSON object for empty or null JSON data
	if len(jsonData) == 0 || jsonDataStr == "null" {
		return "'{}'"
	}

	// Attempt to parse the JSON data to check if it's already valid JSON
	var jsonObj interface{}
	if err := json.Unmarshal(jsonData, &jsonObj); err != nil {
		// If it's not valid JSON, treat it as a simple string and convert it to a JSON string
		return fmt.Sprintf("'%s'", strings.ReplaceAll(jsonDataStr, "'", "''"))
	}

	// If it's valid JSON, return it wrapped in single quotes
	return fmt.Sprintf("%s", jsonDataStr)
}

func main() {
	// Initialize the Redis client
	redisClient = redis.NewClient(&redis.Options{
		Addr:      "vultr-prod-e11d062f-72a7-4c31-ba09-5f4f5194176a-vultr-prod-04c1.vultrdb.com:16752",
		Password:  "AVNS_DVckiitDVxpjeRapPuJ",
		Username:  "default",
		TLSConfig: &tls.Config{
			// Set TLS-related configurations here if needed
		},
	})

	r := mux.NewRouter()
	// Apply the CORS middleware to the router
	r.Use(enableCORS)
	r.HandleFunc("/assign/user/{identifier}", func(w http.ResponseWriter, r *http.Request) {
		assign(w, r, IdentifierTypeUser)
	}).Methods("POST")
	r.HandleFunc("/assign/device/{identifier}", func(w http.ResponseWriter, r *http.Request) {
		assign(w, r, IdentifierTypeDevice)
	}).Methods("POST")
	r.HandleFunc("/variation/user/{identifier}", func(w http.ResponseWriter, r *http.Request) {
		getUserAssignment(w, r, IdentifierTypeUser)
	}).Methods("GET")
	r.HandleFunc("/variation/device/{identifier}", func(w http.ResponseWriter, r *http.Request) {
		getUserAssignment(w, r, IdentifierTypeDevice)
	}).Methods("GET")
	r.HandleFunc("/layers", getLayers).Methods("GET", "OPTIONS")

	// Endpoint for creating or editing experiments
	//r.HandleFunc("/experiment/{name}", upsertExperiment).Methods("POST", "PUT")
	r.HandleFunc("/experiment", upsertExperiment).Methods("GET", "OPTIONS")
	r.HandleFunc("/metrics", getMetrics).Methods("GET") // New endpoint to get metrics
	r.HandleFunc("/experiments", getExperiments).Methods("GET", "OPTIONS")
	r.HandleFunc("/experiment-stats", getExperimentStats).Methods("GET", "OPTIONS")

	http.Handle("/", r)

	// Setup CORS
	corsOpts := handlers.AllowedOrigins([]string{"*"})

	log.Println("Server started on :8080")
	//log.Fatal(http.ListenAndServe(":8080", nil))
	log.Fatal(http.ListenAndServe(":8080", handlers.CORS(corsOpts)(r)))
}
