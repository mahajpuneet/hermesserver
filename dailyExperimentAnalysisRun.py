import pandas as pd
import psycopg2
from scipy.stats import chi2_contingency
import datetime
import numpy as np

# Function to convert numpy data types to native Python types
def convert_numpy_types(row):
    return [item.item() if isinstance(item, np.generic) else item for item in row]

# Database connection details
db_details = "dbname='defaultdb' user='vultradmin' host='vultr-prod-44079b59-5315-41bb-8359-7bb25c3d9946-vultr-prod-04c1.vultrdb.com' password='AVNS_c6g3wFNorBbOZrxBidq' port='16751'"

# Connect to the database
conn = psycopg2.connect(db_details)
cursor = conn.cursor()

# Fetch data from both tables
events_query = "SELECT * FROM userevents;"
assignments_query = "SELECT * FROM experiment_assignments WHERE experiment_name = 'sampleUserExp';"

cursor.execute(events_query)
events_df = pd.DataFrame(cursor.fetchall(), columns=[desc[0] for desc in cursor.description])

cursor.execute(assignments_query)
assignments_df = pd.DataFrame(cursor.fetchall(), columns=[desc[0] for desc in cursor.description])

# Join the dataframes on uid
merged_df = pd.merge(events_df, assignments_df, on="uid")

# Check if merged_df is empty
if merged_df.empty:
    print("No data available for analysis.")
    conn.close()
    exit()

# Calculate click and purchase rates for each bucket
click_rates = merged_df[merged_df.event_type == 'click'].groupby('bucket_variation').size()
purchase_rates = merged_df[merged_df.event_type == 'purchase'].groupby('bucket_variation').size()

# Check if click_rates or purchase_rates is empty
if click_rates.empty or purchase_rates.empty:
    print("Insufficient data for chi-squared test.")
    conn.close()
    exit()

# Chi-square test for statistical significance
chi2, p, _, _ = chi2_contingency([click_rates.values, purchase_rates.values])

# Prepare data for insertion
analysis_date = datetime.date.today()
experiment_name = 'sampleUserExp'

# Function to insert data into the database
def insert_data(cursor, analysis_date, experiment_name, variation, metric_name, metric_value, delta, p_value):
    row = (analysis_date, experiment_name, variation, metric_name, metric_value, delta, p_value)
    converted_row = convert_numpy_types(row)
    cursor.execute("INSERT INTO experiment_analysis_results (analysis_date, experiment_name, bucket_variation, metric_name, metric_value, delta, p_value) VALUES (%s, %s, %s, %s, %s, %s, %s)", converted_row)

# Insert data into the database
for variation in click_rates.index:
    insert_data(cursor, analysis_date, experiment_name, variation, 'click', click_rates[variation], None, p)

for variation in purchase_rates.index:
    insert_data(cursor, analysis_date, experiment_name, variation, 'purchase', purchase_rates[variation], None, p)

# Commit the transaction and close connection
conn.commit()
cursor.close()
conn.close()
